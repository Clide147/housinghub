# SLUHousing

**To change the certificates:**<br>
`go to /bin/www`<br>
`lines 20 through 28 are the lines that are paths to the certificates`<br>
`the certificates are located in /bin/encryption/`<br>
`feel free to rename them`<br>

all of the paths for google should line up fine, I have double checked those paths and they are working on my local machine.<br>
The port is already changed to 443, but if you want to change it to something else, it is in `line 14 ` in case you want to change it.


For local operation run this command first in terminal of webstorm before running:
     `ssh -L 3306:localhost:3306 housinghub.selu.edu -p 4422 -l {username of server}`
This pipes the 3306 port from the local machine to the remote server and that automatically pipes it through to the MySQL server that's running.

Otherwise the Database will not connect

var express = require('express');
var path = require('path');
var logger = require('morgan');
var session = require('express-session');
var passport = require('passport');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var ticketRouter = require('./routes/tickets');
var settingsRouter = require('./routes/settings');
var inspectionRouter = require('./routes/inspection');
var engagementRouter = require('./routes/engagement');
var calendarRoute = require('./routes/googleCalendar');
var mobileRouter = require('./routes/mobileApp');

var app = express();
app.set("view engine", "pug");

app.set("views", path.join(__dirname, "views/Header"));
app.set("views", path.join(__dirname, "views"));



app.use(logger('dev'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true}));
app.use(express.static(path.join(__dirname, './public')));
app.use(session({
    secret: 'sluhousing',
    resave: false,
    saveUninitialized: true
}));

app.use(function(req,res,next){
    res.locals.session = req.session;
    next();
});

app.use(passport.initialize());
app.use(passport.session());


app.use('/', indexRouter);
app.use('/settings', settingsRouter);
app.use('/users', usersRouter);
app.use('/ticket', ticketRouter);
app.use('/inspection', inspectionRouter);
app.use('/engagement', engagementRouter);
app.use('/calendar', calendarRoute.router);
app.use('/mobile', mobileRouter);

module.exports = app;


let nodemailer = require('nodemailer');
let hbs = require('nodemailer-express-handlebars');
let mailOptions ={};
var debug = require('debug');
var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: '465',
    secure: true,
    auth: {
        type:'OAuth2',
        user:'psga6518@selu.edu',
        clientId:'156949929989-339v8o8mmfq00r6jlbk22mhspum1leb2.apps.googleusercontent.com',
        clientSecret:'dwxYM_mIg0rHL_qbO3IW8h8x',
        refreshToken:'1/JuFMJ3BqPxJPDVHjyeYhasdlq1Rb2YjFTm78GA_mBHc',
        accessToken:'ya29.Glt5B81Sa10R1yOhNWJ84NbTFVnVmUQUajCWW-VEZ6EiMNqGLFN0zt_O-iQhQsM64kbCb_BmXMy4cUygVV_N4GueyW1oKGfMC9E6YA_b2GA6up6YvRfOz6ww9HYS',
        expires:3600
    }
});
transporter.use('compile', hbs({viewPath:'HelpFiles/templates', extName:'.hbs'}));

function sendMessageToAdmin(message, host){
    mailOptions = {
        to:'charles@selu.edu',
        subject: 'message from server',
        context:
            {
                message: message,
                host: host
            },
        template: 'message'
    };
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            debug('Email sent: ' + info.response);
        }
    });
}
// We can make more functions for the different kinds of emails we have. But at least
// Everything is in one central location...
function sendTicketEmail(toAddress, firstName, lastName, email, phoneNumber, building, roomNumber, problem, ticketKind,tic_id,b64Image){
    mailOptions.to = toAddress;
    mailOptions.subject = ticketKind+' Ticket: '+tic_id;
    mailOptions.context = {
        ticket_id:tic_id,
        prob:problem,
        rnum:roomNumber,
        building:building,
        phone:phoneNumber,
        email:email,
        lname:lastName,
        fname:firstName,
        ticket:ticketKind
    };
    if(b64Image === null || b64Image === "" || b64Image === undefined){
        mailOptions.attachments = null;
    }
    else {
        mailOptions.attachments = [{
            // encoded string as an attachment
            filename: tic_id + '.png',
            content: b64Image,
            encoding: 'base64'
        }];
    }
    mailOptions.template = "ticket";
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}


function sendHealthSafetyForm(toAddress, Primary_Ra, Secondary_Ra, Building, Floor_Number, Rooms_Info){
    mailOptions.to = toAddress;
    mailOptions.subject = 'This is a test Health and Safety email';
    mailOptions.context = {
        Primary_Ra:Primary_Ra,
        Secondary_Ra:Secondary_Ra,
        Building: Building,
        Floor_Number:Floor_Number,
        Rooms_Info:Rooms_Info
    };
    mailOptions.attachments=null;
    mailOptions.template = "healthsafety";
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}
function sendContactUsForm(toAddress, First_Name, Last_Name, W_Number, Email, Subject, Problem){
    mailOptions.to = toAddress;
    mailOptions.subject = 'This is a test Contact Us email';
    mailOptions.context = {
        fname:First_Name,
        lname:Last_Name,
        wnum:W_Number,
        email:Email,
        sub:Subject,
        problem:Problem
    };
    mailOptions.attachments=null;
    mailOptions.template = "contactus";
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}
module.exports = {
    transporter: transporter,
    mailOptions: mailOptions,
    sendTicketEmail: function (toAddress, firstName, lastName, email, phoneNumber, building, roomNumber, problem, ticketKind,tic_id, b64Image){
        sendTicketEmail(toAddress, firstName, lastName, email, phoneNumber, building, roomNumber, problem, ticketKind,tic_id,b64Image);
    },
    sendHealthSafetyForm: function (toAddress, Primary_Ra, Secondary_Ra, Building, Floor_Number, Rooms_Info) {
        sendHealthSafetyForm(toAddress, Primary_Ra, Secondary_Ra, Building, Floor_Number, Rooms_Info)
    },
    sendContactUsForm: function (toAddress, First_Name, Last_Name, W_Number, Email, Subject, Problem){
        sendContactUsForm(toAddress, First_Name, Last_Name, W_Number, Email, Subject, Problem)
    },
    sendMessageToAdmin: function(message, host){sendMessageToAdmin(message, host)}
};
let db = require('./DBConnect');

OverallBuildings = [
    "Ascension",
    "Twelve Oaks",
    "Pride",
    "Hammond",
    "Louisiana",
    "Taylor",
    "Tangipahoa",
    "Cardinal Newman",
    "Livingston",
    "St. Tammany",
    "Washington",
    "Southeastern Oaks",
    "Greek Village",
];
OaksBuildings = [
    "Building 1",
    "Building 2",
    "Building 3",
    "Building 4",
    "Building 5",
    "Building 6A",
    "Building 6B",
];
GreekBuildings = [
    "Greek Commons",
    "Alpha Sigma Tau",
    "Phi Mu",
    "Sigma Sigma Sigma",
    "Theta Phi Alpha",
    "Alpha Omicrom Pi",
    "Pi Kappa Alpha",
    "Sigma Tau Gamma",
    "Delta Tau Delta",
    "Village M"
];
MaintenanceCategories = [
    "Maintenance",
    "Mold",
    "Pest",
    "Furniture",
    "Internet",
    "Cable"

];
InspectionCategories = [];
AllRoles = [
    "Resident Assistant",
    "Resident",
    "Pro-Staff",
    "Admin",
    "Facilities"
];
temp = [];

var GetContemporaryBuildings = function(callback){
    db.query('Select * from VT_Buildings_Settings',function(err, res,fields){
        //Halls = res.split(',');
        callback(null, res);
    });
};
//Select * from VT_Buildings_Settings
module.exports = {
    OverallBuildings,
    OaksBuildings,
    GreekBuildings,
    MaintenanceCategories,
    InspectionCategories,
    AllRoles,
    temp
};
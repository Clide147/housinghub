function getDate(callback){
    let date = new Date();
    let hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    let min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    let day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    let dateString = year + '-' + month + '-'+day+' '+ hour+':'+min;
    return callback(dateString);
}


module.exports = {
    getDate:function(callback){getDate(callback)}
};
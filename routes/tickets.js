var express = require('express');
var router = express.Router();
var db = require('../HelpFiles/DBConnect');
var emailservice = require('../HelpFiles/EmailService');
var GlobalList = require('../HelpFiles/GlobalLists');
let HelpFunctions = require('../HelpFiles/HelpFunctions');
let Uuid4 = require('uuid/v4');
var Tabulator = require('tabulator-tables');

router.get('/', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query('SELECT * FROM VT_Tickets_NoImg', function (err, result) {
            res.render('Tickets', {ticketList: result});
        });
    }
});

router.get('/category/:category', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query('SELECT * FROM VT_Tickets_'+req.params.category + " WHERE status='Open';", function (err, result) {
            var json = JSON.stringify(result);
            res.render('Tickets', {ticketList: result, tabularTable:json});
        });
    }
});

router.get('/allTickets', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query('SELECT * FROM VT_Tickets_NoImg' + " WHERE status='Open';", function (err, result) {
            var json = JSON.stringify(result);
            res.render('Tickets', {ticketList: result, tabularTable:json});
        });
    }
});

router.get('/ticketForm', function (req, res) {
    var overallBuildings;

    if (!req.session.user) {
        res.render('userLogin')
    } else {
        console.log(req.session);
        res.render('ticketForm', {
            title: "Add",
            AllBuildings: GlobalList.OverallBuildings,
            MaintenanceList: GlobalList.MaintenanceCategories,
            item: {
                fname: req.session.user,
                lname: req.session.lname,
                email: req.session.email,
            },
        });
    }
});

router.post('/ticketform', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {

        let ticket_id = Uuid4();
        let today = "";
        HelpFunctions.getDate(function (res) {
            today = res;
        });
        let base64Data;
        if(req.body.Picture === undefined) {
            base64Data = "";
        }
        else{
            base64Data = req.body.Picture;
        }

        console.log(req.body);
        db.query("Insert into DT_Tickets (ticket_id, fname, lname, email, phone, building, room, problem, category,image,priority, datetime) values (?,?,?,?,?,?,?,?,?,?,?,?)",
            [ticket_id, req.body.fname, req.body.lname, req.body.email, req.body.phone, req.body.building, req.body.room, req.body.problem, req.body.category, base64Data, 'Low', today],
            function (err, result) {
                emailservice.sendTicketEmail(
                    req.body.email,
                    req.body.fname,
                    req.body.lname,
                    req.body.email,
                    req.body.phone,
                    req.body.building,
                    req.body.room,
                    req.body.problem,
                    req.body.category,
                    ticket_id,
                    base64Data
                );
        });
    }
    res.redirect('/');
});

router.get('/ticketedit/:id', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query('SELECT * FROM DT_Tickets WHERE ticket_id=\'' + req.params.id + '\';', function (err, result) {
            res.render('ticketForm', {
                title: "Edit",
                AllBuildings: GlobalList.OverallBuildings,
                MaintenanceList: GlobalList.MaintenanceCategories,
                path: '/ticket/ticketedit/' + result[0].ticket_id,
                item:result[0],
                picture: result[0].image.length
            });
        });
    }
});

router.get('/ticketView/:id', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        console.log('SELECT * FROM DT_Tickets WHERE ticket_id=\'' + req.params.id + '\';');
        db.query('SELECT * FROM DT_Tickets WHERE ticket_id=\'' + req.params.id + '\';', function (err, result) {
            console.log(result[0].image.length);
            res.render('ticketForm', {
                title: "View",
                AllBuildings: GlobalList.OverallBuildings,
                MaintenanceList: GlobalList.MaintenanceCategories,
                disabled: true,
                item:result[0],
                picture: result[0].image.length

            });
        });
    }
});
router.post('/ticketedit/:id', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query("Update DT_Tickets set fname=?, lname=?, email=?, phone=?, building=?, room=?, problem=?, category=?, status=?, priority=? Where ticket_id=?",
            [req.body.fname, req.body.lname, req.body.email, req.body.phone, req.body.building,req.body.room, req.body.problem, req.body.category, req.body.status,req.body.priority, req.params.id],
            function(err,result){
                console.log(result);
            });
        res.redirect('/');
    }
});

module.exports = router;

var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var db = require('../HelpFiles/DBConnect');
var Tabulator = require('tabulator-tables');
var urlencodedParser = bodyParser.urlencoded({ extended: false })

/* GET home page. */
router.get('/', function(req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query('SELECT * FROM DT_Settings;', function (err, result) {
            res.status(200);
            var json = JSON.stringify(result);
            res.render('settings', {settingsList: result, tabularTable: json});
        });
    }
});

router.post('/submit', urlencodedParser, function(req, res){
    console.log(req.body[0].value)

    for(var i = 0; i < req.body.length; i++){
        db.query("UPDATE DT_Settings SET value=? WHERE settings_key=?;",
            [req.body[i].value,req.body[i].settings_key],
            function(err, result){
                if(err) throw err;
                if(result){
                    console.log(result);
                }
            });
    }
});

module.exports = router;

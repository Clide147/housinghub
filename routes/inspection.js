var express = require('express');
var router = express.Router();
var db = require('../HelpFiles/DBConnect');
var uuidv4 = require('uuid/v4');
var GlobalList = require('../HelpFiles/GlobalLists');
let HelpFunctions = require('../HelpFiles/HelpFunctions');
var Tabulator = require('tabulator-tables');
router.get('/', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        var results,results2;
        db.query('SELECT fname,lname from DT_Users WHERE role = "Resident Assistant"', function (err, result) {
            console.log(result);
            results = result;

            db.query('SELECT fname,lname from DT_Users WHERE role = "Resident Assistant"', function (err, result) {
                var tempF,tempL;
                tempF = result[0].fname;
                tempL = result[0].lname;
                console.log(result);
                results2 = result;
                results2[0].fname = result[1].fname;
                results2[0].lname = result[1].lname;
                results2[1].fname = tempF;
                results2[1].lname = tempL;

                res.render('inspection', {
                    userlist: results, secondList: results2,
                    AllBuildings: GlobalList.OverallBuildings
                });
            });


        });


    }
});

router.post('/', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        let hsid = uuidv4();
        let today = "";
        HelpFunctions.getDate(function (res) {
            today = res;
        });
        var RA1 = req.body.ra1.split(' ');
        var RA2 = req.body.ra2.split(' ');
        db.query('Select clientID FROM DT_Users where (fname = ? and lname = ?) Or (fname = ? And lname = ?);',[RA1[0],RA1[1],RA2[0],RA2[1]],function(err, result){
            db.query('INSERT INTO DT_HealthAndSafety (hsid,ra1_clientID,ra2_clientID,building,floor,date) VALUES (?,?,?,?,?,?);',
                [hsid,result[0].clientID, result[1].clientID, req.body.building, req.body.floor, today], function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    //console.log(result);
                    res.status(200);
                    var url = '/inspection/discrepencies/' + hsid;
                    res.redirect(url);
                }
            });
        });
    }
});

router.get('/discrepencies/:id', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        // db.query('SELECT hsid FROM DT_HealthAndSafety WHERE hsid=\'' + req.params.id + '\'', function (err, result) {
        //     console.log('From the Get Function: HSID=', req.params.id);
        //     res.render('discrepencies', {HSID: result[0].HSID,
        //         MaintenanceList: GlobalList.MaintenanceCategories,
        //         AllBuildings: GlobalList.OverallBuildings,});
        // });

       // ^ Dont need that function..

        res.render('discrepencies', {hsid: req.params.id,
            MaintenanceList: GlobalList.MaintenanceCategories,
            AllBuildings: GlobalList.OverallBuildings,});
    }
});

router.post('/discrepencies/:id', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        let today = "";
        HelpFunctions.getDate(function (res) {
            today = res;
        });
        console.log("This is from posting a discrepency: ", req.body);
        // db.query('SELECT hsid FROM DT_HealthAndSafety WHERE hsid=\'' + req.params.id + '\'', function (err, result) {  // You shouldnt need this query, yu can grab hsid from re.params.id
        //     db.query('INSERT INTO DT_HealthAndSafetyIssues (id, hsid, room, category, problem, datetime, priority_level) VALUES (?,?,?,?,?,?,?);',
        //         [uuidv4(), '03de2dfb-5dff-4ac7-a813-2e6a7d79ae06', '105', 'Maintenance', 'kjdfh', 'today', '1'],
        //         function (err, result) {
        //             if (err) {
        //                 console.log(err);
        //                 res.redirect('/');
        //             } else {
        //                 console.log(result);
        //                 res.redirect('/');
        //             }
        //     });
        // });
        db.query('INSERT INTO DT_HealthAndSafetyIssues (id, hsid, room, category, problem, datetime, priority_level) VALUES (?,?,?,?,?,?,?);',
            [uuidv4(), req.params.id, req.body.room, req.body.category, req.body.problem, 'today', 'Low'],
            function (err, result) {
                if (err) {
                    console.log(err);
                    res.redirect('/');
                } else {
                    console.log(result);
                    res.redirect('/inspection/discrepencies/' + req.params.id);
                }
            });
    }
});

router.get('/view/:building', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query('SELECT hsid,ra1_clientID,ra2_clientID, date FROM DT_HealthAndSafety WHERE building = ?',[req.params.building], function (err, result) {
            if (err) {
                console.log(err);
            } else {
                //console.log(result);
                console.log('Success');
                //res.render('viewInspection', {inspectionList: result});
                var json = JSON.stringify(result);
                res.render('viewInspection', {inspectionList: result, tabularTable:json});
            }
        });
    }
});

// router.get('/rooms/:id', function (req, res) {
//     if (!req.session.user) {
//         res.render('userLogin')
//     } else {
//         db.query('SELECT * FROM DT_HealthAndSafety WHERE hsid=?;',[req.body.HSID], function (err, result) {
//             if (err) {
//                 console.log(err);
//             } else {
//                 //console.log(result);
//                 console.log('success');
//                 console.log('SELECT * FROM inspection WHERE hsid=\'' + req.params.id + '\'');
//                 res.render('viewRooms', {roomList: result});
//             }
//         });
//     }
// });

router.get('/rooms/:id', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query('SELECT floor, room FROM DT_HealthAndSafety, DT_HealthAndSafetyIssues WHERE DT_HealthAndSafety.hsid=\'' + req.params.id + '\' AND DT_HealthAndSafety.hsid=DT_HealthAndSafetyIssues.hsid', function (err, result) {
            if (err) {
                console.log(err);
            } else {
                //console.log(result);
                console.log('success');
                console.log('SELECT * FROM DT_HealthAndSafety WHERE hsid=\'' + req.params.id + '\'');
                var json = JSON.stringify(result);
                res.render('viewRooms', {roomList: result, tabularTable:json});
            }
        });
    }
});

router.get('/writeups/:id', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query('SELECT HSID, category, problem FROM DT_HealthAndSafetyIssues WHERE room=\'' + req.params.id + '\'', function (err, result) {
            if (err) {
                console.log(err);
            } else {
                //console.log(result);
                var json = JSON.stringify(result);
                res.render('writeUps', {writeupList: result, tabularTable:json});
            }
        });
    }
});

module.exports = router;
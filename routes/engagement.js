//import * as accounts from "googleapis";

var express = require('express');
var router = express.Router();
var db = require('../HelpFiles/DBConnect');
var emailservice = require('../HelpFiles/EmailService');
var GlobalList = require('../HelpFiles/GlobalLists');
let HelpFunctions = require('../HelpFiles/HelpFunctions');
let uuidv4 = require('uuid/v4');
var util = require('util');
router.get('/', function (req, res) {
    if(!req.session.user){
        res.render('userLogin')
    }else {
        console.log(req.session.date);
        if(req.session.role==='Pro-Staff' || req.session.role==='Admin'){
            db.query('SELECT * FROM DT_Engagement_Rounds, DT_Users WHERE DT_Engagement_Rounds.clientID=DT_Users.clientID', function (err, result){
                console.log("RESULTS: " + result[0].er_id);
                res.render('engagementlist', {thelist: result});
                
            });
        }else if(req.session.role==='Resident Assistant'){
            let userid = req.session.name;
            db.query('SELECT * FROM DT_Engagement_Rounds, DT_Users WHERE DT_Engagement_Rounds.clientID=\''+userid+'\'', function (err, result){
                console.log("RESULTS: " + result);
                res.render('engagementlist', {thelist: result});
                
            });
        }
    }
});

router.get('/viewlist/:id', function (req, res) {
    if(!req.session.user){
        res.render('userLogin')
    }else {
        if(req.session.role==='Pro-Staff' || req.session.role==='Admin'){
            db.query('SELECT * FROM DT_Engagement_Rounds, DT_Users WHERE DT_Engagement_Rounds.clientID=DT_Users.clientID AND building = \''+req.params.id+'\'', function (err, result){
                var json = JSON.stringify(result);
                res.render('engagementlist', {thelist: result, tabularTable: json});

            });
        }else if(req.session.role==='Resident Assistant'){
            let userid = req.session.name;
            db.query('SELECT * FROM DT_Engagement_Rounds, DT_Users WHERE DT_Engagement_Rounds.clientID=DT_Users.clientID AND DT_Users.clientID=\''+userid+'\' AND building = \''+req.params.id+'\'', function (err, result){
                var json = JSON.stringify(result);
                res.render('engagementlist', {thelist: result,tabularTable: json});

            });
        }
    }
});

router.get('/create', function (req, res) {

    res.render('EngagementCheck');
});

router.post('/create', function (req, res) {
    if(!req.session.user){
        res.render('userLogin')
    }else {
        let today;
        HelpFunctions.getDate(function (response) {
            today = response;
        });
        let building = req.body.building;
        let er_id = uuidv4();
        db.query('INSERT INTO DT_Engagement_Rounds (er_id,clientID,date,building) VALUES (?,?,?,?);',[er_id, req.session.name,today,building,] , function (err, result) {
            if (err) {
                console.log('error You suck');
                console.log(err);
            } else {
                console.log(result);
                let url = util.format('/engagement/edit/%s', er_id);
                console.log(url);
;                res.redirect(url);
            }
        });
    }
});
router.get('/edit/:id', function (req, res) {
    if(!req.session.user){
        res.render('userLogin');
    }else {
        console.log("From get: " + req.params.id);
        db.query('SELECT * FROM DT_Engagement_Rounds WHERE er_id=\'' + req.params.id + '\';', function (err, result) {
            console.log("edit: "+result);
            res.render('engagement',{
                title: "Edit",
                er_id: req.params.id,
                clientID: result.clientID,
                building: result.building,
                time1: result.time1,
                time2: result.time2,
                time3: result.time3,
                round1: result.round1,
                round2: result.round2,
                round3: result.round3,
                extLights: result.extLights,
                intLights: result.intLights,
                parLights: result.parLights,
                signs: result.signs,
                doors: result.doors,
                commonAreaSec: result.commonAreaSec,
                commonAreaAcc: result.commonAreaAcc,
                elevator: result.elevator,
                trashRoom: result.trashRoom,
                Maintenance: result.Maintenance,
                addComments: result.addComments,
                AllBuildings: GlobalList.OverallBuildings,
                MaintenanceList: GlobalList.MaintenanceCategories,
                fname: req.session.user,
                lname: req.session.lname,
                email: req.session.email,
            });
        });
    }
});

router.post('/edit/:id', function (req, res) {
    console.log("for post: " + req.params.id);
    if(!req.session.user){
        res.render('userLogin');
    }else {
        let ticket_id = uuidv4();
        let er_id = req.params.id;
        let clientID = req.session.id;
        let building = req.body.building;
        let time1 = req.body.time1;
        let time2 = req.body.time2;
        let time3 = req.body.time3;
        let round1 = req.body.round1;
        let round2 = req.body.round2;
        let round3 = req.body.round3;
        let extLights = ((req.body.extLights === undefined) ? '0' : '1');
        let intLights = ((req.body.intLights === undefined) ? '0' : '1');
        let parLights = ((req.body.parLights === undefined) ? '0' : '1');
        let signs = ((req.body.signs === undefined) ? '0' : '1');
        let doors = ((req.body.doors === undefined) ? '0' : '1');
        let commonAreaSec = ((req.body.commonAreaSec === undefined) ? '0' : '1');
        let commonAreaAcc = ((req.body.commonAreaAcc === undefined) ? '0' : '1');
        let elevator = ((req.body.elevator === undefined) ? '0' : '1');
        let trashRoom = ((req.body.trashRoom === undefined) ? '0' : '1');
        let Maintenance = ((req.body.Maintenance === undefined) ? '0' : '1');
        let addComments = req.body.addComments;

        console.log(er_id);

        db.query("Update DT_Engagement_Rounds set time1=?, time2=?, time3=?, round1=?, round2=?, round3=?, extLights=?, intLights=?, parLights=?, signs=?, doors=?, commonAreaSec=?, commonAreaAcc=?, elevator=?, trashRoom=?, Maintenance=?, addComments=? Where er_id=?",
            [time1, time2, time3, round1, round2, round3, extLights, intLights, parLights, signs, doors, commonAreaSec, commonAreaAcc, elevator, trashRoom, Maintenance, addComments, er_id],
            function(err,result){
                console.log(result);
                if(err){
                    console.log(err);
                }else{
                    console.log(result);
                }
            });
            res.redirect('/');
        }
    });

router.get('/view/:id', function (req, res) {
    if(!req.session.user){
        res.render('userLogin')
    }else {
        db.query('SELECT * FROM DT_Engagement_Rounds WHERE er_id=\'' + req.params.id + '\';', function (err, results) {
            var result = results[0];
            res.render('engagement',{
                title: "View",
                disabled: true,
                er_id: result.er_id,
                clientID: result.clientID,
                building: result.building,
                time1: result.time1,
                time2: result.time2,
                time3: result.time3,
                round1: result.round1,
                round2: result.round2,
                round3: result.round3,
                extLights: result.extLights,
                intLights: result.intLights,
                parLights: result.parLights,
                signs: result.signs,
                doors: result.doors,
                commonAreaSec: result.commonAreaSec,
                commonAreaAcc: result.commonAreaAcc,
                elevator: result.elevator,
                trashRoom: result.trashRoom,
                Maintenance: result.Maintenance,
                addComments: result.addComments,
            });
        });
    }
});

router.post('/filter',function (req,res) {
    if(!req.session.user){
        res.render('userLogin')
    }else {
        let building = req.body.building;
        console.log(building);
        db.query('SELECT * FROM DT_Engagement_Rounds,DT_Users WHERE DT_Engagement_Rounds.clientID=DT_Users.clientID AND building = \'' + building + '\'', function (err, result) {
            res.render('engagementlist', {thelist: result});
        });
    }
});

router.post('/search',function (req, res) {
    if(!req.session.user){
        res.render('userLogin')
    }else {
        let search = req.body.search;

        db.query('SELECT * FROM DT_Engagement_Rounds,DT_Users WHERE round1 LIKE \'%' + search + '%\' OR round2 LIKE \'%' + search + '%\' OR building LIKE \'%' + search + '%\' OR round3 LIKE \'%' + search + '%\' OR addComments LIKE\'%'+search+'%\' AND DT_Engagement_Rounds.clientID=DT_Users.clientID', function (err, result) {
            res.render('engagementlist', {thelist: result});
        });
    }
});


module.exports = router;

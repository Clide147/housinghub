var express = require('express');
var router = express.Router();
var db = require('../HelpFiles/DBConnect');
var {google} = require('googleapis');
var googlelib = require('google-auth-library');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
const passport = require('passport');
var request = require('request');
var role = 'Resident';
var firstTime;
var b = require('../HelpFiles/Buildingmodel');
var HelpFunctions = require('../HelpFiles/HelpFunctions');
var emailservice = require('../HelpFiles/EmailService');
var GlobalLists  = require('../HelpFiles/GlobalLists');
var host = require('os').hostname();
function extractProfile(profile) {
    let imageUrl = '';
    if (profile.photos && profile.photos.length) {
        imageUrl = profile.photos[0].value;
    }
    return {
        id: profile.id,
        displayName: profile.displayName,
        image: imageUrl,
        fname: profile.name.givenName,
        lname: profile.name.familyName,
        email: profile.emails[0].value,
        domain: profile.organization,

    };
}

if(host === 'housinghub'){
    passport.use(new GoogleStrategy({
        clientID: '156949929989-339v8o8mmfq00r6jlbk22mhspum1leb2.apps.googleusercontent.com',
        clientSecret: 'dwxYM_mIg0rHL_qbO3IW8h8x',
        callbackURL: 'https://housinghub.selu.edu/googlelogin',
        accessType: 'offline'
    }, (accessToken, refreshToken, profile, cb) => {
        // Extract the minimal profile information we need from the profile object
        // provided by Google
        cb(null, extractProfile(profile));
    }));
}
else{
    passport.use(new GoogleStrategy({
        clientID: '156949929989-339v8o8mmfq00r6jlbk22mhspum1leb2.apps.googleusercontent.com',
        clientSecret: 'dwxYM_mIg0rHL_qbO3IW8h8x',
        callbackURL: 'https://localhost/googlelogin',
        accessType: 'offline'
    }, (accessToken, refreshToken, profile, cb) => {
        // Extract the minimal profile information we need from the profile object
        // provided by Google
        cb(null, extractProfile(profile));
    }));
}
passport.serializeUser((user, cb) => {
    cb(null, user);
});
passport.deserializeUser((obj, cb) => {
    cb(null, obj);
});
// [END setup]


// [START middleware]
// Middleware that requires the user to be logged in. If the user is not logged
// in, it will redirect the user to authorize the application and then return
// them to the original URL they requested.
function authRequired(req, res, next) {
    if (!req.user) {
        req.session.oauth2return = req.originalUrl;
        return res.redirect('/auth/login');
    }
    next();
}

function addTemplateVariables(req, res, next) {
    res.locals.profile = req.user;
    res.locals.login = `/auth/login?return=${encodeURIComponent(req.originalUrl)}`;
    res.locals.logout = `/auth/logout?return=${encodeURIComponent(req.originalUrl)}`;
    next();
}

router.get('/auth/login', (req, res, next) => {
        if (req.query.return) {
            req.session.oauth2return = req.query.return;
        }
        next();
    },
    passport.authenticate('google', {scope: ['email', 'profile']})
);



router.get('/googlelogin', passport.authenticate('google'), (req, res) => {
    var userid = req.user.id;
    var lname = req.user.lname;
    var email = req.user.email;
    var fname = req.user.fname;

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    }
    if(mm<10){
        mm='0'+mm;
    }
    today = yyyy+'-'+mm+'-'+dd;
    //console.log(userid);
    console.log(role);
    db.query('SELECT * FROM DT_Users WHERE clientID = ?', [userid], function (err, result) {
        if (result[0] === undefined) {

            db.query('INSERT INTO DT_Users (fname,lname,email,clientID,role) VALUES' +
                '(\'' + fname + '\',\'' + lname + '\',\'' + email + '\',\'' + userid + '\',\'' + role + '\');', function (err, result) {
            });
            req.session.name = userid;
            req.session.user = fname;
            emailservice.mailOptions.to = email;
            emailservice.mailOptions.subject = 'Welcome, ' + fname + ', to the SLUHousing App';
            emailservice.mailOptions.text = 'You are currently registered as a resident.  If you are an RA and require that role contact your administrator.'
            emailservice.transporter.sendMail(emailservice.mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
            // res.render('views/Dashboards/Dashboard.pug');
            res.redirect('/');
        }
        else {
            delete req.session.oauth2return;
            fname = result[0].fname;
            role = result[0].role;
            // console.log(role);
            // console.log(role);
            // console.log(req.session);
            req.session.name = userid;
            req.session.user = fname;
            req.session.passport.user.role = role;
            req.session.role = role;
            res.status(200);
            firstTime = false;
            // console.log(req.session.name);
            // console.log('Dashboards/Dashboard'+req.session.role);
            req.session.status = true;
            req.session.date = today;
            req.session.user = req.session.passport.user.fname;
            req.session.lname = req.session.passport.user.lname;
            req.session.name = req.session.passport.user.id;
            req.session.email = req.session.passport.user.email;
            req.session.image = req.session.passport.user.image;
            req.session.role = role;
            // console.log(req.session.passport.user.fname);
            res.redirect('/');
        }
    });
});

router.get('/', function (req, res) {
    db.query(" Select * From VT_Statistics_Main", function (err, result) {
        var moldData = [];
        var pestData = [];
        var furData = [];
        var intData =[];
        var caData = [];
        let MaintData = [];
        if (req.session.status === true) {

            result.forEach(element => {
                if(element.Mold > 0)
                    moldData.push([element.building, element.Mold]);
                if (element.Pest > 0)
                    pestData.push([element.building, element.Pest]);
                if(element.Furniture >0)
                    furData.push([element.building, element.Furniture]);
                if(element.Internet > 0)
                    intData.push([element.building, element.Internet]);
                if(element.Cable > 0)
                    caData.push([element.building, element.Cable]);
                if(element.Maintenance > 0)
                    MaintData.push([element.building, element.Maintenance]);

            });
            if(req.session.role ==='Admin' || req.session.role === 'Pro-Staff') {
                res.render('Dashboards/Dashboard', {
                    moldData: JSON.stringify(moldData),
                    pestData: JSON.stringify(pestData),
                    furData: JSON.stringify(furData),
                    intData: JSON.stringify(intData),
                    caData: JSON.stringify(caData),
                    maintData: JSON.stringify(MaintData)
                });
            }
            else if(req.session.role ==='Resident Assistant'){
                res.render('Dashboards/RADashboard');
            }
            else if(req.session.role ==='Resident'){
                res.render('Dashboards/ResidentDashboard');
            }
            else{
                res.redirect('/ticket/ticketForm');
            }
        } else {
            res.render('userLogin');
        }
    });
});


router.post('/dashboard', function (req, res) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    }
    if(mm<10){
        mm='0'+mm;
    }
    today = yyyy+'-'+mm+'-'+dd;
    // console.log(req.session);
    // console.log(req.session.role);
    // console.log(role);
    req.session.status = true;
    req.session.date = today;
    req.session.user = req.session.passport.user.fname;
    req.session.lname = req.session.passport.user.lname;
    req.session.name = req.session.passport.user.id;
    req.session.email = req.session.passport.user.email;
    req.session.image = req.session.passport.user.image;
    req.session.role = role;
    // console.log(req.session.passport.user.fname);
    res.redirect('/');
});

router.get('/logout', function (req, res) {
    req.session.destroy();
    req.logout();
    res.redirect('/');
});
module.exports = router;


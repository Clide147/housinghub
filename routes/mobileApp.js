let express = require('express');
let router = express.Router();
let db = require('../HelpFiles/DBConnect');
let EmailService = require('../HelpFiles/EmailService');
let HelpFunctions = require('../HelpFiles/HelpFunctions');
let Uuid4 = require('uuid/v4');
let fs = require('fs');

router.post('/contactus/submit', function (req, res) {
    EmailService.sendContactUsForm(
        'mitchell.bosman@selu.edu, ' + req.body.Email,
        req.body.fName,
        req.body.lName,
        req.body.W_Number,
        req.body.Email,
        req.body.Subject,
        req.body.Message,
    );

    res.status(200).send("OK");
});

router.post('/engagement/submit', function (req, res) {
    let er_id = Uuid4();
    let today = HelpFunctions.getDate;
    db.query("Insert into DT_Engagement_Rounds (er_id, clientID, date) values (?, ?, ?)", [er_id, req.body.ClientID, today], function (err, result) {
        if (err) {
            res.send("Not OK");
        } else
            res.status(201)
    });
    db.query("Update DT_Engagement_Rounds set building=?, time1=?, time2=?, time3=?, round1=?, round2=?, round3=?, extLights=?, intLights=?," +
        "parLights=?, signs=?, doors=?, commonAreaSec=?, commonAreaAcc=?, elevator=?, trashRoom=?, Maintenance=?, addComments=? Where er_id=?",
        [req.body.building, req.body.time1, req.body.time2, req.body.time3, req.body.round1, req.body.round2, req.body.round3, req.body.extLights,
            req.body.intLights, req.body.parLights, req.body.signs, req.body.doors, req.body.commonAreaSec, req.body.commonAreaAcc, req.body.elevator,
            req.body.trashRoom, req.body.Maintenance, req.body.addComments, er_id],
        function (err, result) {
            if (err) {
                res.status(409).send("Not OK");
            } else
                res.status(200).send("OK");
        });
});

router.get('/calendar/events', function (req, res) {
    res.redirect('/calendar/events');
});

router.get('/ticket/getMaintenance', function (req, res) {
    db.query('Select * From VT_Tickets_Maintenance_Open',function(err, result){
        res.send(result);
    });
});
router.post('/ticket/retrievePicture', function(req,res){
    db.query('Select image from DT_Tickets where ticket_id=?',[req.body.ticket_id],function(err, result){
        res.send(result[0]);
    });
});
router.post('/ticket/submit', function (req, res) {
    let ticket_id = Uuid4();
    let today;
    HelpFunctions.getDate(function (res) {
        today = res;
    });
    let base64Data;
    if(req.body.image === undefined || req.body.image === null){
        base64Data = "";
    }
    else{
        base64Data = req.body.image;
    }
    // fs.writeFile(ticket_id+'.png', base64Data, 'base64', function(err) {
    //     console.log(err);
    // });
    db.query("Insert into DT_Tickets (ticket_id, fname, lname, email, phone, building, room, problem, category,image,priority, datetime) values (?,?,?,?,?,?,?,?,?,?,?,?)",
        [ticket_id, req.body.fname, req.body.lname, req.body.email, req.body.phone, req.body.building, req.body.room, req.body.problem, req.body.category, base64Data, 'Low', today],
        function (err, result) {
            if (err) {
                console.log(result);
                console.log(err);
                res.status(400).send("Not OK");
            }
            else {
                res.status(200).send("OK");
                EmailService.sendTicketEmail(
                    req.body.email,
                    req.body.fname,
                    req.body.lname,
                    req.body.email,
                    req.body.phone,
                    req.body.building,
                    req.body.room,
                    req.body.problem,
                    req.body.category,
                    ticket_id,
                    base64Data
                );
            }
        });


});
router.post('/ticket/complete', function (req, res) {
    db.query("Update DT_Tickets set status='Closed', fac_comment=? where ticket_id=?",
        [req.body.fac_comment, req.body.ticket_id],
        function (err, result) {
            if (err) {
                console.log(result);
                console.log(err);
                res.status(400).send("Not OK");
            }
            else {
                res.status(200).send();
            }
        });


});

router.post('/hs/submit', function (req, res) {
    var HSID = Uuid4();
    var date = HelpFunctions.getDate;
    db.query("Insert into inspection (HSID, ra1, ra2, building, floor, date) values (?,?,?,?,?,?)",
        [HSID, req.body.ra1, req.body.ra2, req.body.building, req.body.floor, date], function (err, result) {
            if (err)
                res.status(400).send("Not OK");
            else {
                res.status(200).send(HSID);
            }
        });
});
router.post('/hs/finding/:id', function (req, res) {
    let ID = Uuid4();
    db.query("insert into discrepency(ID, HSID, room, category, problem) Values (?,?,?,?,?)",
        [ID, req.params.id, req.body.room, req.body.category, req.body.problem],
        function (err, result) {
            if (err)
                res.status(400).send("Not OK");
            else
                res.status(200).send();
        });
});

router.post('/staff/login', function (req, res) {
    db.query('Select role,clientID from DT_Users where fname=? and lname=? and email=?',
        [req.body.fName, req.body.lName, req.body.email], function (err, results) {
        if (results[0] === undefined)
            res.send("NotOK - Not found");
        else {
            if (results[0] !== "Resident") {
                db.query("Select value From DT_Settings where DT_Settings.key='Housing_App_Password'", function (err, ress) {
                    console.log("key from DT_Settings: " + ress[0].value);
                    if (req.body.password === ress[0].value) {
                        res.status(200).send({
                            Status: "OK",
                            ClientID: results[0].ClientID,
                            Role: results[0].role
                        });
                    }
                });
            }
            else{
                res.status(400).send("NotOK");
            }

        }
    });
});

router.get('/settings/guidebook', function (req, res) {
    db.query('Select * From VT_Guidebook_Settings', function(err, result){
        res.send(result);
    });
});

router.get('/updateLists', function(req,res){
    db.query('select group_concat(building_name) from DT_Buildings', function(err,result){
        res.send(result);
    })
})


module.exports = router;

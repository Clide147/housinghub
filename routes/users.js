var express = require('express');
var router = express.Router();
var db = require('../HelpFiles/DBConnect');
var GlobalList = require("../HelpFiles/GlobalLists");
var Tabulator = require('tabulator-tables');
/* GET users listing. */

router.get('/', function (req, res, next) {
    if (!req.session.user) {
        res.render('userLogin')
    } else if (req.session.role === 'RA' || req.session.role === 'Resident') {
        res.send("you don't have permission");
    } else {
        db.query("SELECT * FROM DT_Users", function (err, result) {
            //res.render('Users/Users', {userList: result});
            var json = JSON.stringify(result);
            res.render('Users/Users', {userList: result, tabularTable: json});
        });
    }
});

router.get('/RA', function (req, res, next) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        console.log("SELECT * FROM DT_Users WHERE role = 'Resident Assistant");
        db.query("SELECT * FROM DT_Users WHERE role = 'Resident Assistant'", function (err, result) {
            //res.render('Users/Users', {userList: result});
            var json = JSON.stringify(result);
            res.render('Users/Users', {userList: result, tabularTable: json});
        });
    }
});

router.get('/ProStaff', function (req, res, next) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query("SELECT * FROM DT_Users WHERE role = 'Pro-Staff'", function (err, result) {
            //res.render('Users/Users', {userList: result});
            var json = JSON.stringify(result);
            res.render('Users/Users', {userList: result, tabularTable: json});
        });
    }
});
router.get('/Admin', function (req, res, next) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query("SELECT * FROM DT_Users WHERE role = 'Admin'", function (err, result) {
            //res.render('Users/Users', {userList: result});
            var json = JSON.stringify(result);
            res.render('Users/Users', {userList: result, tabularTable: json});
        });
    }
});

router.get('/Resident', function (req, res, next) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        db.query("SELECT * FROM DT_Users WHERE role = 'Resident'", function (err, result) {
            //res.render('Users/Users', {userList: result});
            var json = JSON.stringify(result);
            res.render('Users/Users', {userList: result, tabularTable: json});
        });
    }
});

router.get('/View/:id', function (req, res, next) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        console.log(req.session.role);
        console.log('SELECT * FROM DT_Users WHERE clientID=\'' + req.params.id + '\';');
        db.query('SELECT * FROM DT_Users WHERE clientID=\'' + req.params.id + '\';', function (err, result) {
            console.log(result[0].role);
            res.render('Users/SingleUser', {
                AllRoles: GlobalList.AllRoles,
                title: "View User",
                disabled: true,
                fname: result[0].fname,
                lname: result[0].lname,
                Email: result[0].email,
                ClientID: result[0].ClientID,
                role: result[0].role
            });
        });
    }
});

router.get('/Edit/:id', function (req, res, next) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        console.log('SELECT * FROM DT_Users WHERE clientID=\'' + req.params.id + '\';');
        db.query('SELECT * FROM DT_Users where clientID=\'' + req.params.id + '\';', function (err, result) {
            console.log(result);
            res.render('Users/SingleUser', {
                title: "Edit User",
                AllRoles: GlobalList.AllRoles,
                disabled: false,
                fname: result[0].fname,
                lname: result[0].lname,
                Email: result[0].email,
                ClientID: req.params.id,
                role: result[0].role
            });
        });
    }
});

router.post('/Edit/:id', function (req, res) {
    if (!req.session.user) {
        res.render('userLogin')
    } else {
        console.log(req.body);

        var ClientID = req.params.id;
        var role = req.body.role;

        db.query('UPDATE DT_Users SET clientID= ' +
            '\'' + ClientID + '\',role=\'' + role + '\' WHERE clientID=\'' + req.params.id + '\';', function (err, result) {
            console.log(result);
        });
        res.redirect('/users');
    }
});

module.exports = router;
